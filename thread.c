// Biblioteca de subprocesos a nivel de usuario
// INCLUYE
# include  < stdio.h >
# include  < stdlib.h >
# include  < señal.h >
# include  < string.h >
# include  < unistd.h >
# include  < time.h >
# include  < sys / time.h >
# incluye  " threads.h "

// DEFINE
# define  SEGUNDO  1000000
# Definir  MAX_NO_OF_THREADS  100  / * en cualquier estado * /
# define  STACK_SIZE  10000  // 4096
# define  TIME_QUANTUM  1

// Código de caja negra
// ////////////////////////////////////////////////// //////////////////////
# Ifdef __x86_64__
// código para el arco Intel de 64 bits

// typedef unsigned long address_t;
# define  JB_SP  6
# define  JB_PC  7

// Se requiere una traducción cuando se usa la dirección de una variable
// Use esto como un cuadro negro en su código.
address_t  translate_address ( address_t addr)
{
    address_t ret;
    asm  volátil ( " xor     %% fs: 0x30,% 0 \ n "
                 " rol $ 0x11,% 0 \ n "
                 : " = g " (ret)
                 : " 0 " (addr));
    volver ret;
}

# else
// código para el arco Intel de 32 bits

// typedef unsigned int address_t;
# define  JB_SP  4
# define  JB_PC  5

// Se requiere una traducción cuando se usa la dirección de una variable
// Use esto como un cuadro negro en su código.
address_t  translate_address ( address_t addr)
{
    address_t ret;
    asm  volátil ( " xor     %% gs: 0x18,% 0 \ n "
                 " rol $ 0x9,% 0 \ n "
                 : " = g " (ret)
                 : " 0 " (addr));
    volver ret;
}

# endif
// ////////////////////////////////////////////////// //////////////////////

// Variables globales
thread_queue_t * thread_list;     / * la lista de todos los hilos * /
thread_queue_t * ready_list;      / * la lista de todos los hilos listos * /
thread_t * actual;               / * el hilo actual en ejecución * /
int next_thread = 0 ;             / * Se utiliza para asignar ID * /
int scheduling_type;             / * Tipo de programación 0 = RR, 1 = LOTE * /
int clean = 0 ;                   / * si está en limpieza, salga del despacho * /
unsigned start_time = 0 ;         / * Hora en que se inicia un hilo * /

extern  void  thread_enqueue ( thread_t *, thread_queue_t *);
// poner en cola t al final de la cola

extern  thread_t * thread_dequeue ( thread_queue_t *);
// retirarse del frente de la cola

extern  thread_t * Scheduler ();
// implementación del planificador de subprocesos, RR y LOTE

int  CreateThread ( void (* f) ( void ), int prioridad)
{
    // Devuelve -1 si falla
    if (thread_list-> size + 1 > MAX_NO_OF_THREADS)
    {
        printf ( " Demasiados hilos \ n " );
        volver - 1 ;
    }
    thread_t * thread = malloc ( sizeof ( thread_t ));

    thread-> status = malloc ( sizeof ( status_t ));
    thread-> stack = malloc (STACK_SIZE);
    thread-> status -> id = next_thread;
    hilo-> prioridad = prioridad;
    next_thread ++;
    hilo-> estado -> estado = LISTO;

    thread-> sp = ( address_t ) thread- > stack + STACK_SIZE - sizeof ( address_t );
    thread-> pc = ( dirección_t ) f;
    sigsetjmp ( thread- > jbuf , 1 );
    ( thread- > jbuf -> __jmpbuf ) [JB_SP] = traducir_address (thread-> sp );
    ( thread- > jbuf -> __jmpbuf ) [JB_PC] = translate_address (thread-> pc );
    sigemptyset (& thread- > jbuf -> __saved_mask );

    thread_enqueue (thread, ready_list);
    thread_enqueue (thread, thread_list);
    volver thread-> estado -> Identificación ;
}

vacío  InsertWrapper ( thread_t * t, thread_queue_t * q)
{
    if (scheduling_type == 0 )
    {
        thread_enqueue (t, q);
    }
    más
    {
        // InsertAtHead (t, q);
        volver ;
    }
}
// controlador de señal
 Despacho nulo ( int sig)
{
    si (limpio == 1 )
    {
        volver ;
    }
    unsigned time_delta = GetCurrentTime () - start_time;
    status_t * s = actual-> estado ;
    s-> total_exec_time + = time_delta;
    // Guardar estado del hilo actual

    int ret = sigsetjmp ( actual- > jbuf , 1 );
    si (ret == 1 )
    {
        volver ;
    }
    // Itterate a través de la lista de todos los hilos y trata con ellos.
    thread_node_t * node = thread_list-> head ;
    while (nodo! = NULL )
    {
        interruptor (nodo-> hilo -> estado -> estado )
        {
            // no hacer nada
        caso LISTO:
            romper ;
        caso EN EJECUCIÓN:
            romper ;
        caso suspendido:
            romper ;
        caso DORMIR:
            if ( GetCurrentTime ()> = nodo-> hilo -> estado -> wake_time )
            {
                printf ( " tiempo de activación \ n " );
                thread_enqueue (nodo-> thread , ready_list);
                nodo-> hilo -> estado -> estado = LISTO;
            }
            romper ;
        caso ACABADO:
            RemoveFromList ( node- > thread -> status -> id , ready_list);
            romper ;
        }
        nodo = nodo-> siguiente ;
    }
    // Iterar a través de ready_queue para actualizar los tiempos de espera
    thread_node_t * ready = ready_list-> head ;
    while (listo! = NULL )
    {
        ready-> thread -> status -> total_wait_time + = time_delta;
        listo = listo-> siguiente ;
    }

    // Programar nuevo hilo
    if (actual-> estado -> estado == EN EJECUCIÓN)
    {
        InsertWrapper (current, ready_list);
        actual-> estado -> estado = LISTO;
    }
    thread_t * next = GetNextThread ();
    actual = siguiente;
    status_t * stat = next-> status ;
    stat-> state = EN EJECUCIÓN;
stat-     > no_of_bursts ++;
stat-     > avg_exec_time = ( stat- > total_exec_time / stat- > no_of_bursts );
stat-     > avg_wait_time = ( stat- > total_wait_time / ( stat- > no_of_bursts + 1 )); // + 1 b / c num_wait = num_run + 1
hora_inicio     = GetCurrentTime ();
    start_timer ();
    siglongjmp (siguiente-> jbuf , 1 );
}

vacío  Go ()
{
    thread_t * t = GetNextThread ();
    corriente = t;
    t-> estado -> estado = EN EJECUCIÓN;
    start_timer ();
hora_inicio     = GetCurrentTime ();
    siglongjmp (t-> jbuf , 1 );
    mientras que ( 1 );
}

int  GetMyId ()
{
    retorno actual-> estado -> id ;
}

thread_t * GetThread ( int thread_id)
{
    if (thread_list-> head == NULL )
    {
        devuelve  NULL ;
    }

    thread_node_t * node = thread_list-> head ;
    while (node! = NULL && node-> thread -> status -> id ! = thread_id)
    {
        nodo = nodo-> siguiente ;
    }
    if (nodo == NULL )
    {
        devuelve  NULL ;
    }
    volver node-> hilo ;
}

int  DeleteThread ( int thread_id)
{
    int currentId = GetMyId ();
    thread_t * t = GetThread (thread_id);
    si (t == NULL )
    {
        volver - 1 ;
    }

    t-> estado -> estado = FINALIZADO;
    if (thread_id == currentId)
    {
        RendimientoCPU ();
    }
    devuelve  0 ;
}

int  RemoveFromList ( int thread_id, thread_queue_t * q)
{
    thread_node_t * node = q-> head ;
    thread_node_t * prev_node = NULL ;
    // Buscar nodo con el correspondiente thread_id
    while (node-> next ! = NULL && node-> thread -> status -> id ! = thread_id)
    {
        prev_node = nodo;
        nodo = nodo-> siguiente ;
    }
    // Si no se encuentra, devuelve -1
    if (nodo-> hilo -> estado -> id ! = hilo_id)
    {
        volver - 1 ;
    }
    // cabeza
    if (nodo == q-> cabeza )
    {
        // Si se va a eliminar la cabeza.
        q-> cabeza = nodo-> siguiente ;
    }
    // cola
    otra cosa  si (node-> próximo == NULL )
    {
        prev_node-> next = NULL ;
        q-> tail = prev_node;
    }

    // De lo contrario, en el medio / final de la lista
    más
    {
        prev_node-> next = node-> next ;
    }
    libre (nodo);
    devuelve  0 ;
}

 YieldCPU nulo ()
{
    elevar (SIGVTALRM);
}

int  GetStatus ( int thread_id, status_t * status)
{
    thread_t * t = GetThread (thread_id);
    si (t == NULL )
        volver - 1 ;
    estado-> id = t-> estado -> id ;
    estado-> estado = t-> estado -> estado ;
status-     > no_of_bursts = t-> status -> no_of_bursts ;
estado-     > total_exec_time = t-> estado -> total_exec_time ;
estado-     > total_sleep_time = t-> estado -> total_sleep_time ;
status-     > total_wait_time = t-> status -> total_wait_time ;
estado-     > avg_exec_time = t-> estado -> avg_exec_time ;
estado-     > avg_wait_time = t-> estado -> avg_wait_time ;
    volver t-> estado -> id ;
}

int  SuspendThread ( int thread_id)
{
    thread_t * t = GetThread (thread_id);
    si (t == NULL )
        volver - 1 ;
    t-> estado -> estado = SUSPENDIDO;
    RemoveFromList (thread_id, ready_list);
    if (actual-> estado -> id == thread_id)
        RendimientoCPU ();
    return thread_id;
}

int  ResumeThread ( int thread_id)
{
    thread_t * t = GetThread (thread_id);
    si (t == NULL )
        volver - 1 ;
    if (t-> estado -> estado ! = SUSPENDIDO)
        return thread_id;
    thread_enqueue (t, ready_list);
    t-> estado -> estado = LISTO;
    return thread_id;
}

unsigned  GetCurrentTime ()
{
    clock_t  time = clock ();
    unsigned millis = (( double ) ( time ) / CLOCKS_PER_SEC) * 1000 ;
    volver millis;
}

vacío  SleepThread ( int sec)
{
    actual-> estado -> estado = DORMIR;
    current-> status -> wake_time = ( GetCurrentTime () + (sec * 1000 ));
    current-> status -> total_sleep_time + = (sec * 1000 );
    RendimientoCPU ();
}

 configuración nula ( int horario)
{
    srand ( tiempo ( NULL ));
    scheduling_type = horario; // RR = 0, LOTE == 1, FCFS == 2
    ready_list = malloc ( sizeof ( thread_queue_t ));
    ready_list-> head = ready_list-> tail = NULL ;
    ready_list-> size = 0 ;

    thread_list = malloc ( sizeof ( thread_queue_t ));
    thread_list-> head = ready_list-> tail = NULL ;
    thread_list-> size = 0 ;

    actual = NULL ;

    señal (SIGVTALRM, envío);
}


void  start_timer ()
{
    struct itimerval tv;
    televisión. it_value . tv_sec = TIME_QUANTUM;     // hora del primer contador de tiempo
    televisión. it_value . tv_usec = 0 ;               // hora del primer contador de tiempo
    televisión. it_interval . tv_sec = 0 ;             // tiempo de todos los temporizadores excepto el primero
    televisión. it_interval . tv_usec = 0 ;            // tiempo de todos los temporizadores excepto el primero
    setitimer (ITIMER_VIRTUAL, & tv, NULL );
}


// Obtener el encabezado de la cola de hilos
thread_t * GetNextThread ()
{
	 planificador de retorno ();
}

/ * Imprime el estado de todos los hilos y mata todos los hilos.
En otras palabras, capture el último estado de todos los hilos. * /

 Limpieza nula ()
{
    limpio = 1 ;
    // Imprimir contenidos de la estructura de estado
    thread_node_t * node = thread_list-> head ;
    while ( NULL ! = nodo)
    {
        thread_t * t = nodo-> thread ;
        status_t * s = t-> estado ;
        printf ( " thread % d info: \ n " , s-> id );
        interruptor (s-> estado )
        {
            caso (EN EJECUCIÓN):
                printf ( " estado del hilo = EN EJECUCIÓN \ n " );
                romper ;
            caso (LISTO):
                printf ( " estado del hilo = LISTO \ n " );
                romper ;
            caso (DORMIR):
                printf ( " estado del hilo = DORMIR \ n " );
                romper ;
            caso (SUSPENDIDO):
                printf ( " estado del hilo = SUSPENDIDO \ n " );
                romper ;
            estuche (ACABADO):
                printf ( " estado del hilo = ACABADO \ n " );
                romper ;
        }
        printf ( " num_runs = % d \ n total_exec_time = % d \ n "
               " total_sleep_time = % d \ n total_wait_time = % d \ n avg_exec_time = % d \ n avg_wait_time = % d \ n " , s-> no_of_bursts , s-> total_exec_time , s-> total_sleep_time , s-> total_wait_time , s-> avg_exec_time , s-> avg_wait_time );
        nodo = nodo-> siguiente ;
    }
    // eliminar todos los hilos
    // libera readyQ
    nodo = ready_list-> head ;
    thread_node_t * next;
    while ( NULL ! = nodo)
    {
        siguiente = nodo-> siguiente ;
        RemoveFromList ( node- > thread -> status -> id , ready_list);
        nodo = siguiente;
    }
    libre (lista_lista);
    // libera hilos
    nodo = hilo_lista-> cabeza ;
    while ( NULL ! = nodo)
    {
        siguiente = nodo-> siguiente ;
        thread_t * t = nodo-> thread ;
        RemoveFromList (nodo-> hilo -> estado -> id , hilo_lista);
        libre (t-> pila );
        libre (t-> estado );
        libre (t);
        nodo = siguiente;
    }
    libre (lista de hilos);
    // salir
    salida ( 0 );
}